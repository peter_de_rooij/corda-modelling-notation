# Corda Modelling Notation

[PlantUML](http://plantuml.com/) macros to create [Corda Modelling Notation](https://solutions.corda.net/corda-modelling-notation/overview/overview-overview.html) Diagrams

# Usage

## Clone
Clone this repository.
## Use
In your Corda project, where you want to include the macros, use the relative path.  Like this:
```
!@startuml
include ../../../corda-modelling-notation/macros/CordaStateEvolution.puml

CORDA_TX(AgreeDeal, Agree Deal)
CORDA_TX(UpdateDeal, Update Deal)

START_STATE(start)

...

@enduml
```

# Known Limitations

* It provides only an approximation of the diagrams at [Corda Modelling Notation](https://solutions.corda.net/corda-modelling-notation/overview/overview-overview.html) 
* The Corda C logo used in the swimlanes is included via the raw Bitbucket URL.  This is not very fast and may not work all that well when offline.
  You can change this by changing the `LOGO_URL` definition in `CordaSwimlanes.puml` to point to the copy on your hard disk.  Make sure this is the absolute path! 
* I have not (yet?) identified a reasonable way to make these macros work straight from PlantUML.  (One is to include the macros via the raw URL of the bitbucket sources, something like 
```
!include https://bitbucket.org/peter_de_rooij/corda-modelling-notation/raw/a69de19cb90a4f9bce9e913801ee6c34e9fb5bcb/macros/CordaStateEvolution.puml
```
